'use strict';
var chem_input = document.getElementById("chem-inp");
var chem = document.getElementById("chem");
var table_container = document.getElementById("table-container");
var table = [
    "H", "Hydrogen", "1.00794", 1, 1,
    "He", "Helium", "4.002602", 18, 1,
    "Li", "Lithium", "6.941", 1, 2,
    "Be", "Beryllium", "9.012182", 2, 2,
    "B", "Boron", "10.811", 13, 2,
    "C", "Carbon", "12.0107", 14, 2,
    "N", "Nitrogen", "14.0067", 15, 2,
    "O", "Oxygen", "15.9994", 16, 2,
    "F", "Fluorine", "18.9984032", 17, 2,
    "Ne", "Neon", "20.1797", 18, 2,
    "Na", "Sodium", "22.98976...", 1, 3,
    "Mg", "Magnesium", "24.305", 2, 3,
    "Al", "Aluminium", "26.9815386", 13, 3,
    "Si", "Silicon", "28.0855", 14, 3,
    "P", "Phosphorus", "30.973762", 15, 3,
    "S", "Sulfur", "32.065", 16, 3,
    "Cl", "Chlorine", "35.453", 17, 3,
    "Ar", "Argon", "39.948", 18, 3,
    "K", "Potassium", "39.948", 1, 4,
    "Ca", "Calcium", "40.078", 2, 4,
    "Sc", "Scandium", "44.955912", 3, 4,
    "Ti", "Titanium", "47.867", 4, 4,
    "V", "Vanadium", "50.9415", 5, 4,
    "Cr", "Chromium", "51.9961", 6, 4,
    "Mn", "Manganese", "54.938045", 7, 4,
    "Fe", "Iron", "55.845", 8, 4,
    "Co", "Cobalt", "58.933195", 9, 4,
    "Ni", "Nickel", "58.6934", 10, 4,
    "Cu", "Copper", "63.546", 11, 4,
    "Zn", "Zinc", "65.38", 12, 4,
    "Ga", "Gallium", "69.723", 13, 4,
    "Ge", "Germanium", "72.63", 14, 4,
    "As", "Arsenic", "74.9216", 15, 4,
    "Se", "Selenium", "78.96", 16, 4,
    "Br", "Bromine", "79.904", 17, 4,
    "Kr", "Krypton", "83.798", 18, 4,
    "Rb", "Rubidium", "85.4678", 1, 5,
    "Sr", "Strontium", "87.62", 2, 5,
    "Y", "Yttrium", "88.90585", 3, 5,
    "Zr", "Zirconium", "91.224", 4, 5,
    "Nb", "Niobium", "92.90628", 5, 5,
    "Mo", "Molybdenum", "95.96", 6, 5,
    "Tc", "Technetium", "98", 7, 5,
    "Ru", "Ruthenium", "101.07", 8, 5,
    "Rh", "Rhodium", "102.9055", 9, 5,
    "Pd", "Palladium", "106.42", 10, 5,
    "Ag", "Silver", "107.8682", 11, 5,
    "Cd", "Cadmium", "112.411", 12, 5,
    "In", "Indium", "114.818", 13, 5,
    "Sn", "Tin", "118.71", 14, 5,
    "Sb", "Antimony", "121.76", 15, 5,
    "Te", "Tellurium", "127.6", 16, 5,
    "I", "Iodine", "126.90447", 17, 5,
    "Xe", "Xenon", "131.293", 18, 5,
    "Cs", "Caesium", "132.9054", 1, 6,
    "Ba", "Barium", "132.9054", 2, 6,
    "La", "Lanthanum", "138.90547", 4, 9,
    "Ce", "Cerium", "140.116", 5, 9,
    "Pr", "Praseodymium", "140.90765", 6, 9,
    "Nd", "Neodymium", "144.242", 7, 9,
    "Pm", "Promethium", "145", 8, 9,
    "Sm", "Samarium", "150.36", 9, 9,
    "Eu", "Europium", "151.964", 10, 9,
    "Gd", "Gadolinium", "157.25", 11, 9,
    "Tb", "Terbium", "158.92535", 12, 9,
    "Dy", "Dysprosium", "162.5", 13, 9,
    "Ho", "Holmium", "164.93032", 14, 9,
    "Er", "Erbium", "167.259", 15, 9,
    "Tm", "Thulium", "168.93421", 16, 9,
    "Yb", "Ytterbium", "173.054", 17, 9,
    "Lu", "Lutetium", "174.9668", 18, 9,
    "Hf", "Hafnium", "178.49", 4, 6,
    "Ta", "Tantalum", "180.94788", 5, 6,
    "W", "Tungsten", "183.84", 6, 6,
    "Re", "Rhenium", "186.207", 7, 6,
    "Os", "Osmium", "190.23", 8, 6,
    "Ir", "Iridium", "192.217", 9, 6,
    "Pt", "Platinum", "195.084", 10, 6,
    "Au", "Gold", "196.966569", 11, 6,
    "Hg", "Mercury", "200.59", 12, 6,
    "Tl", "Thallium", "204.3833", 13, 6,
    "Pb", "Lead", "207.2", 14, 6,
    "Bi", "Bismuth", "208.9804", 15, 6,
    "Po", "Polonium", "209", 16, 6,
    "At", "Astatine", "210", 17, 6,
    "Rn", "Radon", "222", 18, 6,
    "Fr", "Francium", "223", 1, 7,
    "Ra", "Radium", "226", 2, 7,
    "Ac", "Actinium", "227", 4, 10,
    "Th", "Thorium", "232.03806", 5, 10,
    "Pa", "Protactinium", "231.0588", 6, 10,
    "U", "Uranium", "238.02891", 7, 10,
    "Np", "Neptunium", "237", 8, 10,
    "Pu", "Plutonium", "244", 9, 10,
    "Am", "Americium", "243", 10, 10,
    "Cm", "Curium", "247", 11, 10,
    "Bk", "Berkelium", "247", 12, 10,
    "Cf", "Californium", "251", 13, 10,
    "Es", "Einstenium", "252", 14, 10,
    "Fm", "Fermium", "257", 15, 10,
    "Md", "Mendelevium", "258", 16, 10,
    "No", "Nobelium", "259", 17, 10,
    "Lr", "Lawrencium", "262", 18, 10,
    "Rf", "Rutherfordium", "267", 4, 7,
    "Db", "Dubnium", "268", 5, 7,
    "Sg", "Seaborgium", "271", 6, 7,
    "Bh", "Bohrium", "272", 7, 7,
    "Hs", "Hassium", "270", 8, 7,
    "Mt", "Meitnerium", "276", 9, 7,
    "Ds", "Darmstadium", "281", 10, 7,
    "Rg", "Roentgenium", "280", 11, 7,
    "Cn", "Copernicium", "285", 12, 7,
    "Uut", "Unutrium", "284", 13, 7,
    "Fl", "Flerovium", "289", 14, 7,
    "Uup", "Ununpentium", "288", 15, 7,
    "Lv", "Livermorium", "293", 16, 7,
    "Uus", "Ununseptium", "294", 17, 7,
    "Uuo", "Ununoctium", "294", 18, 7
];

var elements =[];
var masses = {};// renamed from 'weights' because weight is a force!
for (var i = 0;i <table.length;i+=5){
    elements.push(table[i]);
    masses[table[i]] = parseFloat(table[i+2]);
}
elements.sort(function(a,b){return b.length-a.length});
function gcd(a,b){
    while(b){
        var t= b;
        b = a%b;
        a=t;
    }
    return a;
}
function lcm(a,b){
    return Math.abs(a*b)/gcd(a,b);
}
function isChar(char){
    return char&&typeof char ==="string" &&char.length === 1;
  }
function isAlpha(char){
    return isChar(char)&&/[a-zA-Z]/.test(char[0]);
}
function isLower(str){
    return isAlpha(str) &&str === str.toLowerCase() ;
}
function isNum(char){
    return isChar(char)&&/[0-9]/.test(char[0]);
  }
function assert(bool,msg){
    chem.innerHTML = '<div class="error" style="background-color:#ffced0;color:#800800">'+"<b>Error:</b> "+msg+'</div>';
    if(!bool)throw msg;
}
class Fraction{
    constructor(num,denom){
        this.numerator = num;
        this.denominator = denom;
        assert(this.denominator!=0,"Division by zero!");
        assert(isFinite(this.numerator),"Fraction numerator should be finite!");
        assert(isFinite(this.denominator),"Fraction denominator should be finite!");
        this._reduce();
        this.value=this.numerator/this.denominator;
        this.int = Math.trunc(this.value);
        Object.freeze(this);
    }
    static convert(arg){
        if(arg instanceof Fraction)return arg;
        assert(!isNaN(arg),"Fraction could not be NaN!");
        var n = +arg;
        var d = 1;
        while(Math.trunc(n)!==n){
            n*=10;
            d*=10;
        }
        return new Fraction(n,d)
    }
    _reduce(){
        var g = gcd(this.numerator,this.denominator);
        this.numerator/= g;
        this.denominator/= g;
        
    }
    multiply(other){
        other = Fraction.convert(other);
        var n = other.numerator*this.numerator;
        var d = other.denominator*this.denominator;
        return new Fraction(n,d);
    }
    divide(other){
        other = Fraction.convert(other);
        var n = other.denominator*this.numerator;
        var d = other.numerator*this.denominator;
        return new Fraction(n,d);
    }
    add(other){
        other = Fraction.convert(other);
        var d = (this.denominator*other.denominator);
        var n = this.numerator*other.denominator + other.numerator*this.denominator;
        
        return new Fraction(n,d);
    }
    subtract(other){
        other = Fraction.convert(other);
        var d = (this.denominator*other.denominator);
        var n = this.numerator*other.denominator - other.numerator*this.denominator;
        
        return new Fraction(n,d);
    }
    toString(){
        return ""+this.numerator+"/"+this.denominator;
    }

}
class AugMatrix{
    constructor(matrix){
        this.matrix = matrix.map(x=>x.map(y=>Fraction.convert(y)));
    }
    subtract_complement(a,b,x){
        var fk= this.matrix[b][x].divide(this.matrix[a][x]);
        for(var i =0;i<this.matrix[b].length;i++){
            this.matrix[b][i]=this.matrix[b][i].subtract(fk.multiply(this.matrix[a][i])); 
        }
        return this;

    }
    swap_rows(a,b){
        var t = this.matrix[a];
        this.matrix[a]=this.matrix[b];
        this.matrix[b]=t;
        
    }
    find_row_with_max_abs_column(col,restricted=[]){
        var max_abs_val=0;
        var max_index=0;
        for(var i=0;i<this.matrix.length;i++)
            if(Math.abs(this.matrix[i][col].value)>max_abs_val&&!restricted.includes(i)){
                max_abs_val = Math.abs(this.matrix[i][col].value);
                max_index=i;
            }
        return max_index;
    }
    pivot(){
        var restrict =[];
        for(var i = 0;i<this.matrix.length;i++){
            if(this.matrix[i][i].value==0.0){
                this.swap_rows(i,this.find_row_with_max_abs_column(i,restrict));
                restrict.push(i);
            }
            
        }
        return this.matrix;
    }
    toString(){
        return this.matrix.map(x=>"["+x.map(y=>y.toString()).join(",")+"]").join("\n")+"";
    }
}
function gauss(aug){
    function swap_rows(mat,a,b){
        var t = mat[a];
        mat[a]=mat[b];
        mat[b]=t;
        return mat;
    }


    var coefficients =[];
    var aug_matrix = new AugMatrix(aug);
    console.log(aug_matrix.toString());
    var matrix = aug_matrix.pivot();
    console.log(aug_matrix.toString());
    for(var i =0;i<matrix.length;i++)
        for(var j =0;j<matrix.length;j++)
            if(i!=j&&matrix[i][i].value != 0){
                console.log(matrix[i][i].toString());
                console.log(matrix[j][i].toString());
                console.log(matrix[i].toString());
                console.log(matrix[j].toString())
                aug_matrix.subtract_complement(i,j,i);
            }
    matrix = aug_matrix.matrix;          
    console.log(aug_matrix.toString());
    for(var i =0;i<matrix.length;i++)
        coefficients.push(matrix[i][matrix[i].length-1].divide(matrix[i][i]).multiply(-1));
    coefficients.push(Fraction.convert(1));
    console.log(coefficients);
    
        
    return coefficients;

    

}


function make_integer(coefficients){
    var denoms = coefficients.map(x=>x.denominator);
    var lcmAll = denoms.reduce(lcm);
    
    return coefficients.map(x=>x.multiply(lcmAll).value);

}
function get_molecular_mass(parsed_molecule){
    var mass = 0;
    for(var atom in parsed_molecule){
        mass+=masses[atom]*parsed_molecule[atom];
    }
    return mass;
}
function parse(eqn){
    var parsed=[];
    var par_side=[];
    for(var side of (eqn.split("\u2192"))){
        //console.log(side)
        par_side=[];
        for(var molecule of side.split("+")){
            
            var re =  /([A-Z][a-z]*)([0-9]*)/g;
            var bracket_re = /\(([^(]+?)\)(\d*)/g;
            
            var ml= molecule.replace(/\s+/g,"");
            //HACK: will will probably break at large numbers
            while(bracket_re.test(ml))ml = ml.replace(bracket_re,function(g0,g1,g2){return g1.repeat(parseInt(g2||1))});
            console.log(ml);
            var obj={};
            var mtch;


            while((mtch = re.exec(ml))!=null){
                //console.log(mtch+":"+ml);
                obj[mtch[1]]=(obj[mtch[1]])?obj[mtch[1]]:0;
                obj[mtch[1]]+=(+mtch[2])?(+mtch[2]):1;
                
            }
            par_side.push(obj);
        }
        parsed.push(par_side);
    }
   
    
    return parsed.slice(0,2);
}

function get_pretty_molecules(user_input){
   return new Array( user_input.replace(/\d+/g,"<sub>$&</sub>").split(/\u2192|\+/g))[0];
}


function insertCoefficients(eqn,coefficients){
    var c =0;
    var text = [];
    eqn = eqn.replace(/\d+/g,"<sub>$&</sub>");
    for(var side of (eqn.split(" \u2192 "))){
        var cmol = [];
        for(var molecule of side.split(" + ")){
            cmol.push("<b>"+coefficients[c++]+"</b> "+molecule);
        }
        text.push(cmol.join(" + "))
    }
    return text.join(" \u2192 ");
}
function createTable(array_of_objects){
    var table = document.createElement("table");
    var thead = document.createElement("thead");
    var thr = document.createElement("tr");
    var tbody = document.createElement("tbody");
    var columns = Object.keys(array_of_objects[0]).sort();
    for(var col of columns){
        var th = document.createElement("th");
        th.innerHTML = col;
        thr.appendChild(th)
    }
    thead.appendChild(thr);
    table.appendChild(thead);
    for(var obj of array_of_objects){
        var tr = document.createElement("tr");
        for(var prop of columns){
            var td = document.createElement("td");
            if(obj[prop] instanceof HTMLElement){
                td.appendChild(obj[prop]);
            }else{
                td.innerHTML = obj[prop]?  obj[prop].toString():'';
            }
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    table.appendChild(tbody);
    return table;
}
function constructMatrix(parsed){
    function listAllAtoms(side){
        var atoms = new Set();
        for(var mol of side){
            for(var a in mol)
                atoms.add(a);
        }
        return Array.from(atoms);
    }
    var atoms = listAllAtoms(parsed[0]).sort();
    var atoms2= listAllAtoms(parsed[1]).sort();

    console.log("All atoms:"+atoms.toString());
    var matrix =[];
    assert(atoms.length===atoms2.length,"Atom counts are unequal!");
    for(var i=0;i<atoms.length;i++)
        assert(atoms[i]===atoms2[i],"There are different atoms in both sides!");
    
    assert(parsed.length==2,"Invalid equation length!");
    for(var atom of atoms){ 
        var eqn = [];
        for(var i=0;i<parsed.length;i++){ 
            var m = 1-i*2;
            var side = parsed[i]; 
            for(var molecule of side){
                var coefficient = molecule[atom]===undefined?0:molecule[atom]*m;
                eqn.push(coefficient);
            }
        } 
        matrix.push(eqn);
    }
    
    return matrix;

}
function normalize_overdetermined_system(mat){
    function are_equations_equivalent(a,b){
        var k = a[0]/b[0];
        for(var i =1;i<a.length;i++){
            if((a[i]!==0||b[i]!==0)&&a[i]/b[i]!==k)return false;
        }
        return true;
    }
    for(var i =0;i < mat.length;i++)
        for(var j=i+1;j<mat.length;j++)
            if(are_equations_equivalent(mat[i],mat[j]))
                mat.splice(j--,1);
        
    
    while(mat.length>mat[0].length-1)mat.pop();
    return mat;
}
function displayTables(){
    table_container.innerHTML='';
    for(var i=0;i<arguments.length;i++)
        table_container.appendChild(createTable(arguments[i]));
}
function functionFactory(){
    var fun = arguments[0];
    var args = Array.prototype.slice.call(arguments,1);
    return function () {
        return fun.apply(this,args.concat(Array.prototype.slice.call(arguments)));
    }
 }
function updateMass(index,inputs,coefficients,molmasses,evt){
    var molar_mass = molmasses[index];
    var k = parseFloat(inputs[index].value)/molar_mass/coefficients[index];
    
    for(var i=0;i<inputs.length;i++){
        if (i==index)continue;
        var input = inputs[i];
        input.value = k*coefficients[i]*molmasses[i];
    }
} 
function constructTable(pretty,parsed){

    var data=[];
    var mass_inputs=[];
    var molMasses=[];
    var p = parsed[0].concat(parsed[1]);
    for(var i=0;i<pretty.length;i++){
        var mass_input = document.createElement("input");
        
        mass_inputs.push(mass_input);
        var molMass = get_molecular_mass(p[i]);
        molMasses.push(molMass);
        var row= {Name:pretty[i],"Relative mass":molMass,"mass, kg":mass_input};
        
        data.push(row);
    }
    return {data:data,mass_inputs:mass_inputs,molMasses:molMasses};
}
function process(eqn){
    var parsed = parse(eqn);
    console.log(eqn)
    var molecules = get_pretty_molecules(eqn);
    assert(parsed.length==2,"Not a valid reaction equation");
    //If there are any error logged in the console, then it probably won't work
    var matrix = constructMatrix(parsed);
    
    ///We'll just let the last coefficient be one for now
    //overdetermined system case
    normalize_overdetermined_system(matrix);
    console.log(matrix);
    console.log(new AugMatrix(matrix).toString())
    //Solve this system
    var coefficients = gauss(matrix);
    coefficients = make_integer(coefficients);
    var result = insertCoefficients(eqn,coefficients);
    var table_data = constructTable(molecules,parsed);
    var mass_inputs= table_data.mass_inputs;
    var molMasses = table_data.molMasses;
    var data = table_data.data;
    displayTables(data);
    for(var i =0;i<mass_inputs.length;i++)mass_inputs[i].addEventListener("change",functionFactory(updateMass,i,mass_inputs,coefficients,molMasses));
    return result;
}

function anyStartsWith(str,arr){
    for(var elem of arr){
        if(elem.startsWith(str))return true;
    }
    return false;
}
function suggest(text,key){
    
    var i = text.length - 1;
    var last = "";
    do{
        if(text[i])last=text[i]+last;
    }while(text[i]&&isLower(text[i--]));
    //console.log(last) 
   
    if(anyStartsWith(last+key.toLowerCase(),elements))
        return text+key.toLowerCase();
    else
        return text + key.toUpperCase();

}
function toArray(l){
    if(l.length!==undefined){
      return Array.prototype.slice.call(l,0);
    }
  }
function presser(evt){
    if(!getSelection().anchorNode)return;
    if(toArray(getSelection().anchorNode.childNodes).some(x =>{return x instanceof HTMLInputElement;}))return;
    if(evt.which === 13){
        chem.innerHTML=process(chem_input.innerText);
        chem_input.innerHTML="";
        return;
    }else if(evt.which==187){
        chem_input.innerHTML+=evt.shiftKey?" + ":" &rarr; ";
    }else if(evt.which==57&&evt.shiftKey){//shift+9
        chem_input.innerHTML+="(";
    }else if(evt.which==48&&evt.shiftKey){//shift+0
        chem_input.innerHTML+=")";
    }else if(evt.which>47&&evt.which<=57){
        chem_input.innerHTML+="<sub>"+String.fromCharCode(evt.which)+"</sub>";
    }else{
        if(evt.which==8){//backspace
            chem_input.innerHTML=chem_input.innerText.substr(0,chem_input.innerText.length-1).replace(/\d+/g,"<sub>$&</sub>");
            
        }else{
            if(evt.shiftKey&&evt.which!=16){//is not shift key
                chem_input.innerHTML+=String.fromCharCode(evt.which).toUpperCase();
            }else if(evt.which!=16){
                chem_input.innerHTML=suggest(chem_input.innerHTML,String.fromCharCode(evt.which));
                
            }
           
        }

    }
    
    
   
}
function fallback_process(){
    chem.innerHTML=process(chem_input.value.replace(/\+/g," + ").replace("="," \u2192 "));
    chem_input.value="";
    if(table_container.children[0]){
        table_container.children[0].style.margin = "auto";
        table_container.children[0].style.fontSize = "1.5em";
        table_container.querySelectorAll("input").forEach(x=>x.style.fontSize="1em");
    }
}
function replaceElement(el,new_el){
    for(var attr of el.attributes){
        new_el.setAttribute(attr.name,attr.value);
    }
    el.parentNode.replaceChild(new_el,el);
}
function isMobile(){
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1) || (navigator.userAgent.indexOf('Android') !== -1) || (navigator.userAgent.indexOf('iPhone') !== -1) || (navigator.userAgent.indexOf('Mobile') !== -1);
}
function fallback(){
    var f_input = document.createElement("input");
    f_input.id = "chem-inp";
    replaceElement(chem_input,f_input);
    chem.style.fontSize="2em";
    chem_input = f_input;
    chem_input.addEventListener("change",fallback_process);
    
}
if(isMobile()){
    fallback();

}else{
    document.body.addEventListener("keydown",presser);
}
